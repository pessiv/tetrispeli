//tämän on tarkoitus olla tasohyppelytetriksen päätiedosto, johon toiminnallisuuksia pistetään
var c = document.getElementById("tasohyppelytetris");
var audio = new Audio('./tetris.mp3');
var ctx = c.getContext("2d");
var chartexture = document.getElementById("hahmo");
var blocktexture = document.getElementById("palikka");
var blocksy = 0;
var blocksx = 50;
var charx = 100;
var chary = 333;
var pisteet = 0;
var blockx = [];
var blocky = [];
function displayer(){
  this.updateScreen = function(){
    ctx.clearRect(0,0,750,500);
    ctx.fillStyle = "#ADD8E6";
    ctx.fillRect(0, 0, 750, 500);
    ctx.fillStyle = "brown";
    ctx.fillRect(0,460,c.width,500);
    console.log("screen was refreshed");
    ctx.drawImage(chartexture,charx,chary);
    ctx.drawImage(blocktexture,blocksx,blocksy);
    ctx.font = "30px Arial";
    pisteet += 1;
    ctx.fillText("Score: "+(pisteet/100),10,50);
  }
}
function dead(){
  alert("player died at score: "+(pisteet/100));
  pisteet = 0;
  charx = 50;
  blocksy = 0;
  blocksx = 50;
  blocky = [];
  blockx = [];
}
function mainloop(){
  audio.play();
  blocksy += 3;
  if(blocksy>395){
    pisteet += 5000
    blockx.push(blocksx);
    blocky.push(blocksy);
    blocksy = 0;
  }
  screenupdater.updateScreen();
  if(blocky[0] != null){
  ctx.drawImage(blocktexture,blockx[0],blocky[0]);
}
if(blocky[1] != null){
  ctx.drawImage(blocktexture,blockx[1],blocky[1]);
}
if(blocky[2] != null){
  ctx.drawImage(blocktexture,blockx[2],blocky[2]);
}
if(blocky[3] != null){
  ctx.drawImage(blocktexture,blockx[3],blocky[3]);
}
if(blocky[4] != null){
  ctx.drawImage(blocktexture,blockx[4],blocky[4]);
}
if(blocky[5] != null){
  ctx.drawImage(blocktexture,blockx[5],blocky[5]);
}
if(blocky[6] != null){
  ctx.drawImage(blocktexture,blockx[6],blocky[6]);
}
if(blocky[7] != null){
  ctx.drawImage(blocktexture,blockx[7],blocky[7]);
}
if(blocky[8] != null){
  ctx.drawImage(blocktexture,blockx[8],blocky[8]);
}
  if(blocky[9] != null){
    ctx.drawImage(blocktexture,blockx[9],blocky[9]);
  }
  if(blocky[10] != null){
    ctx.drawImage(blocktexture,blockx[10],blocky[10]);
  }
  if(blocky[11] != null){
    ctx.drawImage(blocktexture,blockx[11],blocky[11]);
  }
  if(blocky[12] != null){
    ctx.drawImage(blocktexture,blockx[12],blocky[12]);
  }
  if(blocky[13] != null){
    ctx.drawImage(blocktexture,blockx[13],blocky[13]);
  }
  if(blocky[14] != null){
    ctx.drawImage(blocktexture,blockx[14],blocky[14]);
  }
  if(blocky[15] != null){
    ctx.drawImage(blocktexture,blockx[15],blocky[15]);
  }
  if(blocky[16] != null){
    ctx.drawImage(blocktexture,blockx[16],blocky[16]);
  }
  if(blocky[17] != null){
    ctx.drawImage(blocktexture,blockx[17],blocky[17]);
  }
  if(blocky[18] != null){
    blockx = [];
    blocky = [];
  }
  if(charx - blocksx < 10 && chary - blocksy < 15 && charx - blocksx >= 0){
    dead();
  }
  if(chary < 333){
    chary += 3;
  }
  if(charx < 0){
    dead();
  }
  if(charx > (c.width-64)){
    dead();
  }
  requestAnimationFrame(mainloop);
}
var screenupdater = new displayer();
document.addEventListener('keydown', function(event) {
  if (event.keyCode == 37){
    charx -= 7;
    pisteet += 500;
    console.log(1);
  }
  else if (event.keyCode == 38) {
    if(chary >= 333){
      chary -= 180;
      pisteet += 500;
      console.log(2);
    }
  }
  else if (event.keyCode == 39) {
    charx += 7;
    pisteet += 500;
    console.log(3);
  }
  else if (event.keyCode == 40) {
    //chary += 15;
    console.log(4);
  }
  else if (event.keyCode == 65) {
    blocksx -= 40
    pisteet += 500;
    console.log(6);
  }
  else if (event.keyCode == 83) {
    blocksy += 30;
    pisteet += 500;
    console.log(7);
  }
  else if (event.keyCode == 68) {
    blocksx += 40;
    pisteet += 500;
    console.log(8);
  }
}, true);